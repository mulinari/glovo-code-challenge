This is a Java8 Spring Boot project. Get familiar with it and import it into your IDE.

- Make sure you're able to execute it by using `./run` or just running `com.glovoapp.backender.API::main()`
- Make sure you know how to add a Unit test to the project using Junit5
- Make sure you know how to execute the test suite
- Make sure you know how to add a new endpoint
- Make sure you know how to use application.properties and how to inject configuration

After that, start with the [WORDING](./WORDING.md). Good luck :)!



#Work done...

 

- Added Lombok to the project
- Added Spring boot test for MVC testing
- Implemented Filter design pattern in order to chain the requirements in a decoupled way 
- 92% Line coverage
- No implementation of JPA Specification but would be a good thing
if we were using database, it would extract filtering from java code and would
improve performance. I have not implemented because I beleave the focus is on the functionality and
ideally I should not change the code already done. 
https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#specifications

- I tried to change as less as possible the code already implemented but I would separate these classes into packages
to make it easier to find them.

- Same for Spring hateoas, would be a good approach to implement to make it a good rest full service.
But again, just wanted not to change the code already existent.
https://spring.io/guides/tutorials/rest/


Hope you like and Thank you for the opportunity!\
Adriano Mulinari


