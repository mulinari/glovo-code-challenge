package com.glovoapp.backender;

public interface CourierService {
    Courier getCourier(String id);
}
