package com.glovoapp.backender;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class VipCriteria implements Criteria {

    @Override
    public List<Order> meetCriteria(Courier courier, List<Order> orders) {
        List<Order> filteredOrders = orders.stream()
                .filter(order -> order.getVip())
                .collect(Collectors.toList());

        sortOrders(courier, filteredOrders);
        return filteredOrders;
    }
}
