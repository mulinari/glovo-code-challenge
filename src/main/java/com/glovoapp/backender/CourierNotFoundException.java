package com.glovoapp.backender;

public class CourierNotFoundException extends RuntimeException {

    public CourierNotFoundException() {
        super("Courier Not found");
    }
}
