package com.glovoapp.backender;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RestCriteria implements Criteria {

    @Override
    public List<Order> meetCriteria(Courier courier, List<Order> orders) {
        List<Order> filteredOrders = orders.stream()
                .filter(order -> !order.getFood() && !order.getVip())
                .collect(Collectors.toList());

        sortOrders(courier, filteredOrders);
        return filteredOrders;
    }
}
