package com.glovoapp.backender;

import java.util.List;

import static com.glovoapp.backender.DistanceCalculator.calculateDistance;

public interface Criteria {
    List<Order> meetCriteria(Courier courier, List<Order> orders);

    default void sortOrders(Courier courier, List<Order> orders) {
        orders.sort((o1, o2) ->
                calculateDistance(courier.getLocation(), o1.getPickup()) >
                        calculateDistance(courier.getLocation(), o2.getPickup()) ? 1 : -1);
    }
}
