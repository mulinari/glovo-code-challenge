package com.glovoapp.backender;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.glovoapp.backender.DistanceCalculator.calculateDistance;

@Component
@AllArgsConstructor
public class SlotCriteria implements Criteria {

    private final ExperimentConfig experimentConfig;
    private final PriorityCriteria priorityCriteria;

    @Override
    public List<Order> meetCriteria(Courier courier, List<Order> orders) {
        List<Order> slottedOrders = new ArrayList<>();
        List<Order> slot;

        Integer multiplier = 1;
        Double maxDistance = 0.0;

        do {
            maxDistance = experimentConfig.getSlotSize() * multiplier;
            slot = filterByDistance(courier, orders, (maxDistance - experimentConfig.getSlotSize()), maxDistance);
            slottedOrders.addAll(priorityCriteria.meetCriteria(courier, slot));

            multiplier++;
        } while (!slot.isEmpty());

        return slottedOrders;
    }

    private List<Order> filterByDistance(Courier courier, List<Order> orders, Double minDistance, Double maxDistance) {
        return orders.stream().filter(order ->
                calculateDistance(courier.getLocation(), order.getPickup()) < maxDistance &&
                        calculateDistance(courier.getLocation(), order.getPickup()) > minDistance)
                .collect(Collectors.toList());
    }
}
