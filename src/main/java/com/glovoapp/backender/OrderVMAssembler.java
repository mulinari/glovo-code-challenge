package com.glovoapp.backender;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderVMAssembler {
    public List<OrderVM> toOrderVMList(List<Order> orders) {
        return orders.stream()
                .map(order -> new OrderVM(order.getId(), order.getDescription()))
                .collect(Collectors.toList());
    }
}
