package com.glovoapp.backender;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class BicycleCriteria implements Criteria {

    private final ExperimentConfig experimentConfig;

    @Override
    public List<Order> meetCriteria(Courier courier, List<Order> orders) {
        return orders.stream().filter(order ->
                DistanceCalculator.calculateDistance(courier.getLocation(), order.getPickup())
                        < experimentConfig.getDistanceLimit()).collect(Collectors.toList());
    }
}
