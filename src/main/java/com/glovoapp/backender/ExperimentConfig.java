package com.glovoapp.backender;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Data
@Component
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "experiment")
public class ExperimentConfig {
    private Set<String> inBoxOnly;
    private List<String> priority;
    private Double slotSize;
    private Double distanceLimit;
}
