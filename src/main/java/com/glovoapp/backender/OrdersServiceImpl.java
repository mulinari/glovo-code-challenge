package com.glovoapp.backender;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class OrdersServiceImpl implements OrdersService {

    private final ExperimentConfig experimentConfig;
    private final CourierService courierService;
    private final OrderRepository orderRepository;
    private final BicycleCriteria bicycleCriteria;
    private final SlotCriteria slotCriteria;

    @Override
    public List<Order> getOrders() {
        return findAllAvailableOrders();
    }

    @Override
    public List<Order> getOrders(String courierId) {
        Courier courier = courierService.getCourier(courierId);
        List<Order> orders;

        if (hasBox(courier))
            orders = findAllAvailableOrders();
        else
            orders = findNonBoxOrders();

        if (bicycle(courier))
            orders = filterConfiguredLimit(courier, orders);

        return slotAndPrioritize(courier, orders);
    }

    private List<Order> slotAndPrioritize(Courier courier, List<Order> orders) {
        return slotCriteria.meetCriteria(courier, orders);
    }

    private List<Order> filterConfiguredLimit(Courier courier, List<Order> orders) {
        return bicycleCriteria.meetCriteria(courier, orders);
    }

    private List<Order> findNonBoxOrders() {
        return orderRepository.findAll(experimentConfig.getInBoxOnly());
    }

    private List<Order> findAllAvailableOrders() {
        return orderRepository.findAll();
    }

    private boolean hasBox(Courier courier) {
        return courier.getBox();
    }

    private boolean bicycle(Courier courier) {
        return Vehicle.BICYCLE.equals(courier.getVehicle());
    }

}
