package com.glovoapp.backender;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import static java.util.Optional.ofNullable;

@Component
@AllArgsConstructor
public class CourierServiceImpl implements CourierService {

    private final CourierRepository courierRepository;

    @Override
    public Courier getCourier(String id) {
        return ofNullable(courierRepository.findById(id))
                .orElseThrow(CourierNotFoundException::new);
    }
}
