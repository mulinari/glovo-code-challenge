package com.glovoapp.backender;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
public class PriorityCriteria implements Criteria {

    private ExperimentConfig experimentConfig;
    private VipCriteria vipCriteria;
    private FoodCriteria foodCriteria;
    private RestCriteria restCriteria;

    @Override
    public List<Order> meetCriteria(Courier courier, List<Order> orders) {
        List<Order> prioritized = new ArrayList<>();

        experimentConfig.getPriority().forEach(priority -> {
            if (priority.equalsIgnoreCase(APIConstants.VIP))
                prioritized.addAll(vipCriteria.meetCriteria(courier, orders));

            if (priority.equalsIgnoreCase(APIConstants.FOOD))
                prioritized.addAll(foodCriteria.meetCriteria(courier, orders));

        });
        prioritized.addAll(restCriteria.meetCriteria(courier, orders));

        return prioritized;
    }
}
