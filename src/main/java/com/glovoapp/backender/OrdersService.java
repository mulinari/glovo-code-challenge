package com.glovoapp.backender;

import java.util.List;

public interface OrdersService {
    List<Order> getOrders(String courierId);
    List<Order> getOrders();
}
