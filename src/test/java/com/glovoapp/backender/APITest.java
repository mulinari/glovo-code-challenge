package com.glovoapp.backender;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = API.class)
@AutoConfigureMockMvc
public class APITest {

    private static final String ORDERS = "/orders";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenRootPage_thenShouldReturnWelcomeMessage() throws Exception {
        this.mockMvc.perform(get(""))
                .andExpect(status().isOk())
                .andExpect(content().string("Welcome to Adriano's Test API"));
    }

    @Test
    public void whenGetOrders_thenShouldReturnAllAvailableOrders() throws Exception {
        this.mockMvc.perform(get(ORDERS))
                .andExpect(status().isOk());
    }

    @Test
    public void givenValidCourierId_whenGetOrders_thenShouldReturnOrders() throws Exception {
        this.mockMvc.perform(get(ORDERS + "/courier-faa2186e65f2"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("order-9f737450c86f")));
    }

    @Test
    public void givenInvalidCourierId_whenGetOrders_thenThrowCourierNotFoundException() throws Exception {
        this.mockMvc.perform(get(ORDERS + "/1"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("Courier Not found")));
    }
}