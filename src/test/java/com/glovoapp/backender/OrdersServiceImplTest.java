package com.glovoapp.backender;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OrdersServiceImplTest {

    private ExperimentConfig experimentConfig;
    private CourierService courierService;
    private OrderRepository orderRepository;
    private OrdersService ordersService;
    private BicycleCriteria bicycleCriteria;
    private VipCriteria vipCriteria;
    private FoodCriteria foodCriteria;
    private RestCriteria restCriteria;
    private PriorityCriteria priorityCriteria;
    private SlotCriteria slotCriteria;

    private static final Integer TOTAL_AVAILABLE_ORDERS = 101;

    @BeforeAll
    public void setUp() {

        experimentConfig = new ExperimentConfig(
                Stream.of("flamingo", "pizza", "cake").collect(Collectors.toSet()),
                Stream.of("vip", "food").collect(Collectors.toList()),
                0.500, 5.0);

        courierService = new CourierServiceImpl(new CourierRepository());
        orderRepository = new OrderRepository();
        bicycleCriteria = new BicycleCriteria(experimentConfig);
        vipCriteria = new VipCriteria();
        foodCriteria = new FoodCriteria();
        restCriteria = new RestCriteria();
        priorityCriteria = new PriorityCriteria(
                experimentConfig,
                vipCriteria,
                foodCriteria,
                restCriteria);
        slotCriteria = new SlotCriteria(experimentConfig, priorityCriteria);

        ordersService = new OrdersServiceImpl(
                experimentConfig,
                courierService,
                orderRepository,
                bicycleCriteria,
                slotCriteria);
    }

    @Test()
    public void givenInvalidCourier_then_shouldThrowCourierNotFoundException() {
        assertThrows(CourierNotFoundException.class, () -> {
            ordersService.getOrders("1");
        });
    }

    //## Hide #################################################################################
    /*
    If the description of the order contains the words pizza, cake or flamingo,
    we can only show the order to the courier if they are equipped with a Glovo box.
     */
    @Test
    public void givenCourierIdWithNoGlovoBox_then_shouldNotReturnPreConfiguredOrders() {
        //Given
        Courier noBoxCourier = courierService.getCourier("courier-7e1552836a04");

        //When
        List<Order> orders = ordersService.getOrders(noBoxCourier.getId());

        //Then
        orders.forEach(order -> {
            experimentConfig.getInBoxOnly().forEach(description ->
                    assertFalse(order.getDescription().toLowerCase().contains(description)));
        });
    }

    @Test
    public void givenCourierIdWithGlovoBox_then_shouldReturnAllAvailableOrders() {
        //Given
        Courier boxedCourier = courierService.getCourier("courier-d190ca23f070");

        //When
        List<Order> orders = ordersService.getOrders(boxedCourier.getId());

        //Then
        assertEquals(TOTAL_AVAILABLE_ORDERS, orders.size());
    }

    /*
    If the order is further than 5km to the courier, we will only show it to
    couriers that move in motorcycle or electric scooter.
     */
    @Test
    public void givenBicycleCourier_then_shouldReturnOnlyOrdersNoFurtherThanConfiguredDistance() {

        //Given
        Courier bicycleCourier = courierService.getCourier("courier-e996a3a5bab6");

        //When
        List<Order> orders = ordersService.getOrders(bicycleCourier.getId());

        //Then
        orders.forEach(order -> {
            assertTrue(DistanceCalculator.calculateDistance(
                    bicycleCourier.getLocation(), order.getPickup()) < experimentConfig.getDistanceLimit());
        });
    }

    @Test
    public void givenMotorCycleScooterCourier_then_shouldReturnAllAvailableOrders() {

        //Given
        Courier motorCycleCourier = courierService.getCourier("courier-2c9b954a95fa");

        //When
        List<Order> orders = ordersService.getOrders(motorCycleCourier.getId());

        //Then
        assertEquals(TOTAL_AVAILABLE_ORDERS, orders.size());

    }

    @Test
    public void givenElectricScooterCourier_then_shouldReturnAllAvailableOrders() {

        //Given
        Courier motorCycleCourier = courierService.getCourier("courier-ce556fdac553");

        //When
        List<Order> orders = ordersService.getOrders(motorCycleCourier.getId());

        //Then
        assertEquals(TOTAL_AVAILABLE_ORDERS, orders.size());

    }

    //## Prioritise ###########################################################################

    /*
    We'll show the orders that are close to the courier first, in slots of 500
    meters (e.g. orders closer than 500m have the same priority; same orders
    between 500 and 1000m)

    Inside each slot, we'll show the orders that belong to a VIP customer first, sorted by distance
     */
    @Test
    public void givenCourier_then_shouldReturnSlottedOrdersWithVIPOnesFirst() {
        //Given
        Courier courier = courierService.getCourier("courier-ce556fdac553");

        //When
        List<Order> orders = ordersService.getOrders(courier.getId());

        //Then
        assertEquals(orders.get(6).getVip(), true); //vip
        assertEquals(orders.get(7).getVip(), true); //vip
        assertEquals(orders.get(8).getFood(), true); //food
        assertEquals(orders.get(9).getFood(), true); //food
        assertEquals(orders.get(10).getFood(), true); //food
        assertEquals(orders.get(11).getFood(), true); //food

    }

    /*
    Then, in each slot, we'll show the orders that are food, sorted by distance
     */
    @Test
    public void givenCourier_then_shouldReturnSlottedOrdersFoodOrderedByDistance() {
        //Given
        Courier courier = courierService.getCourier("courier-ce556fdac553");

        //When
        List<Order> orders = ordersService.getOrders(courier.getId());

        //Then
        assertEquals(orders.get(0).getFood(), true); //food
        assertEquals(orders.get(1).getFood(), true); //food
        assertEquals(orders.get(2).getFood(), true); //food
        assertEquals(orders.get(3).getFood(), true); //food
        assertEquals(orders.get(4).getFood(), true); //food
        assertEquals(orders.get(5).getFood(), false); //food
        assertEquals(orders.get(5).getVip(), false); //rest
/*
        int index = 0;
        for (Order order : orders) {
            System.out.println("index=" + index + " ");
            if (order.getVip()) System.out.print("vip");
            if (order.getFood()) System.out.print("food");
            if (!order.getVip() && !order.getFood()) System.out.print("rest");
            System.out.print("    ---------  "+(DistanceCalculator.calculateDistance(courier.getLocation(), order.getPickup())));
            System.out.println();
            index++;
        }
*/
    }

    /*
    ...and the rest of orders in the slot, sorted by distance
     */
    @Test
    public void givenCourier_then_shouldReturnSlottedOrdersNotFoodOrderedByDistance() {

        //Given
        Courier courier = courierService.getCourier("courier-ce556fdac553");

        //When
        List<Order> orders = ordersService.getOrders(courier.getId());

        //Then
        for (int i = 0; i < 15; i++) {
            if (i < 6) {
                assertTrue(DistanceCalculator.calculateDistance(courier.getLocation(), orders.get(i).getPickup()) < 0.500);
            }
            if (i > 5) {
                assertTrue(DistanceCalculator.calculateDistance(courier.getLocation(), orders.get(i).getPickup()) > 0.500);
            }
        }

    }
}